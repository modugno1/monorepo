# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.12](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.11...@modugno1/log-lib@1.0.12) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.11](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.10...@modugno1/log-lib@1.0.11) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.10](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.9...@modugno1/log-lib@1.0.10) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.9](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.8...@modugno1/log-lib@1.0.9) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.8](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.7...@modugno1/log-lib@1.0.8) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.7](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.6...@modugno1/log-lib@1.0.7) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.6](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.5...@modugno1/log-lib@1.0.6) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.5](https://gitlab.com/modugno1/monorepo/compare/@modugno1/log-lib@1.0.3...@modugno1/log-lib@1.0.5) (2021-10-22)

**Note:** Version bump only for package @modugno1/log-lib





## [1.0.15](https://gitlab.com/modugno1/monorepo/compare/@monorepo/log-lib@1.0.14...@monorepo/log-lib@1.0.15) (2020-10-22)

**Note:** Version bump only for package @monorepo/log-lib





## [1.0.14](https://gitlab.com/modugno1/monorepo/compare/@monorepo/log-lib@1.0.13...@monorepo/log-lib@1.0.14) (2020-10-22)

**Note:** Version bump only for package @monorepo/log-lib





## [1.0.13](https://gitlab.com/modugno1/monorepo/compare/@monorepo/log-lib@1.0.12...@monorepo/log-lib@1.0.13) (2020-10-22)

**Note:** Version bump only for package @monorepo/log-lib





## 1.0.12 (2020-10-22)

**Note:** Version bump only for package @monorepo/log-lib





## [1.0.11](https://gitlab.com/modugno1/monorepo/compare/@monolib/log-lib@1.0.10...@monolib/log-lib@1.0.11) (2020-10-22)

**Note:** Version bump only for package @monolib/log-lib





## 1.0.10 (2020-10-22)

**Note:** Version bump only for package @monolib/log-lib





## [1.0.9](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.8...@my-monorepo/log-lib@1.0.9) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.8](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.7...@my-monorepo/log-lib@1.0.8) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.7](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.6...@my-monorepo/log-lib@1.0.7) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.6](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.5...@my-monorepo/log-lib@1.0.6) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.5](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.4...@my-monorepo/log-lib@1.0.5) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.4](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.3...@my-monorepo/log-lib@1.0.4) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.3](https://gitlab.com/modugno1/monorepo/compare/@my-monorepo/log-lib@1.0.2...@my-monorepo/log-lib@1.0.3) (2020-10-22)

**Note:** Version bump only for package @my-monorepo/log-lib





## [1.0.2](https://github.com/modugno/monorepo-js/compare/@my-monorepo/log-lib@1.0.1...@my-monorepo/log-lib@1.0.2) (2020-10-04)

**Note:** Version bump only for package @my-monorepo/log-lib





## 1.0.1 (2020-10-04)

**Note:** Version bump only for package @my-monorepo/log-lib

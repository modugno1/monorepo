# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.6](https://gitlab.com/modugno1/monorepo/compare/@modugno1/math-lib@1.0.1...@modugno1/math-lib@1.0.6) (2021-10-22)

**Note:** Version bump only for package @modugno1/math-lib





## [1.0.8](https://gitlab.com/modugno1/monorepo/compare/@monorepo/math-lib@1.0.7...@monorepo/math-lib@1.0.8) (2020-10-22)

**Note:** Version bump only for package @monorepo/math-lib





## [1.0.7](https://gitlab.com/modugno1/monorepo/compare/@monorepo/math-lib@1.0.6...@monorepo/math-lib@1.0.7) (2020-10-22)

**Note:** Version bump only for package @monorepo/math-lib





## [1.0.6](https://gitlab.com/modugno1/monorepo/compare/@monorepo/math-lib@1.0.5...@monorepo/math-lib@1.0.6) (2020-10-22)

**Note:** Version bump only for package @monorepo/math-lib





## 1.0.5 (2020-10-22)

**Note:** Version bump only for package @monorepo/math-lib





## [1.0.4](https://gitlab.com/modugno1/monorepo/compare/@monolib/math-lib@1.0.3...@monolib/math-lib@1.0.4) (2020-10-22)

**Note:** Version bump only for package @monolib/math-lib





## 1.0.3 (2020-10-22)

**Note:** Version bump only for package @monolib/math-lib





## [1.0.2](https://github.com/modugno/monorepo-js/compare/@my-monorepo/math-lib@1.0.1...@my-monorepo/math-lib@1.0.2) (2020-10-04)

**Note:** Version bump only for package @my-monorepo/math-lib





## 1.0.1 (2020-10-04)

**Note:** Version bump only for package @my-monorepo/math-lib
